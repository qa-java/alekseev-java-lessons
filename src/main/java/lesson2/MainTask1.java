package lesson2;

public class MainTask1 {

    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        System.out.println(a + "," + b);

        int temp = a;

        a = b;
        b = temp;
        System.out.println(a + "," + b);
    }

}
