package lesson2;

public class MainTask3 {
    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 55, 99, 22, 75, 88};
        int max = numbers[0];
        int min = numbers[0];
        int sum = 0;
        for (int number : numbers) {
            if (number > max) {
                max = number;
            }
            if (number < min) {
                min = number;
            }
            sum += number;
        }
        int middle = sum/numbers.length;
        System.out.println("min= " + min + ", max= " + max + ", avg= "+middle);
    }
}
