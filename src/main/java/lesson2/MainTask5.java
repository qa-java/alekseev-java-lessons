package lesson2;

public class MainTask5 {
    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 3, 4, 5, 1, 2, 3, 4, 5};
        int numberToDelete = 3;

        int newLength = numbers.length;
        for (int number : numbers) {
            if (number == numberToDelete) {
                newLength--;
            }
        }

        int[] numbersWithNewLength = new int[newLength];

        int counter = 0;
        for (int number : numbers) {
            if (number != numberToDelete) {
                numbersWithNewLength[counter] = number;
                counter++;
            }
        }

        for (int element : numbersWithNewLength) {
            System.out.println(element);
        }
    }

}

