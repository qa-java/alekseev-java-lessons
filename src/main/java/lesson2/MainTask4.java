package lesson2;

public class MainTask4 {
    public static void main(String[] args) {
        int start = 2;
        int end = 200;

        for (int i = start; i <= end; i++) {
            if (getTrueIfNumberIsPrime(i)) {
                System.out.println(i);
            }
        }

    }

    private static boolean getTrueIfNumberIsPrime(int number) {
        for (int i = 3; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

}
