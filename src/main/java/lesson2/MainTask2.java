package lesson2;

public class MainTask2 {
    public static void main(String[] args) {
        String[] plate = new String[]{"apple1", "apple2", "apple3", "apple4", "apple5", "apple6", "apple7", "apple8", "apple9", "apple10"};
        int luckyApple = 8;

        for (int i = 0; i <= plate.length; i++) {
            if (i == luckyApple - 1) {
                System.out.println(plate[i] + " this is lucky");
            }
        }

    }
}
