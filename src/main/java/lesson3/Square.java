package lesson3;

import java.awt.*;

public class Square extends Figure {
    private int side;
    private Color color;

    public Square(int side, Color color) {
        this.side = side;
        this.color = color;
    }


    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    @Override
    public float getSquare() {
        return (side * side);
    }

    @Override
    public float getPerimetr() {
        return side * 4;
    }

    @Override
    public void printGlass() {
    }

    @Override
    public void printPaper() {
        System.out.println("I'm paper");
    }

    @Override
    public String toString() {
        return "Quadrate: " +
                "side = " + side +
                ", color = '" + color + '\'' +
                "  { Perimetr: " + getPerimetr() + " sm }" +
                "  { Square: " + getSquare() + " sm² }";
    }
}