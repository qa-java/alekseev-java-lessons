package lesson3;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Box {
    public static void main(String[] args) {
        List<Triangle> triangleList = new ArrayList<>();
        triangleList.add(new Triangle(5, 6, 7, Color.BLACK));
        triangleList.add(new Triangle(10, 8, 6, Color.DARK_GRAY));
        triangleList.add(new Triangle(9, 9, 7, Color.GREEN));


        List<Circle> circleList = new ArrayList<>();
        circleList.add(new Circle(5, Color.PINK));
        circleList.add(new Circle(8, Color.BLACK));
        circleList.add(new Circle(10, Color.BLUE));


        List<Square> quadrateList = new ArrayList<>();
        quadrateList.add(new Square(6, Color.WHITE));
        quadrateList.add(new Square(3, Color.CYAN));
        quadrateList.add(new Square(9, Color.GRAY));
        quadrateList.remove(1);

        List<Object> figures = new ArrayList<>();
        figures.addAll(triangleList);
        figures.addAll(circleList);
        figures.addAll(quadrateList);

        for (Object figure : figures) {
            System.out.println(figure);
        }
    }
}

