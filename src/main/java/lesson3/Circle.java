package lesson3;

import java.awt.*;

public class Circle extends Figure {

        private int radius;
        private Color color;


        public Circle(int radius, Color color) {
            this.radius = radius;
            this.color = color;
        }


        public int getRadius() {
            return radius;
        }

        public void setRadius(int radius) {
            this.radius = radius;
        }

        @Override
        public float getSquare() {
            return (float) (Math.PI * (radius * radius));
        }

        @Override
        public float getPerimetr() {
            return (float) (Math.PI * radius * 2);
        }

        @Override
        public void printGlass() {
            System.out.println("i'm glass");
        }

        @Override
        public void printPaper() {
            System.out.println("i'm paper");
        }

        @Override
        public String toString() {
            return "Circle: " +
                    "color = '" + color + '\'' +
                    ", radius = " + radius +
                    "  { Perimetr: " + getPerimetr() + " sm }" +
                    "  { Square: " + getSquare() + " sm² }" ;
        }
    }


