package lesson3;

import java.awt.*;

public class Triangle extends Figure {
    private int sideA;
    private int sideB;
    private int sideC;
    private Color colors;

    public Triangle(int sideA, int sideB, int sideC, Color colors) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        this.colors = colors;
    }

    public String getSideABC() {
        return ("Side A: "
                + Integer.toString(sideA)
                + ", Side B: "
                + Integer.toString(sideB)
                + ", Side C: "
                + Integer.toString(sideC));
    }

    public void setSideABC(int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }


    @Override
    public String getColor() {
        return colors.toString();
    }

    @Override
    public float getSquare() {
        return (float) (Math.sqrt((getPerimetr() / 2) * (((getPerimetr() / 2) - sideA)
                * ((getPerimetr() / 2 - sideB) * ((getPerimetr() / 2) - sideC)))));
    }

    @Override
    public float getPerimetr() {
        return sideA + sideB + sideC;
    }

    @Override
    public void printGlass() {
        System.out.println("i'm glass");
    }

    @Override
    public void printPaper() {

    }


    @Override
    public String toString() {
        return "Triangle: " +
                "sideA = " + sideA +
                ", sideB = " + sideB +
                ", sideC = " + sideC +
                ", color = '" + colors + '\'' +
                "  { Perimetr: " + getPerimetr() + " sm }" +
                "  { Square: " + getSquare() + " sm² }";
    }
}