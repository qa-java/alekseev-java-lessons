package lesson3;

import java.awt.*;

public abstract class Figure implements Paper, Glass {
    private Colors color;


    public abstract float getSquare();

    public abstract float getPerimetr();


    public String getColor() {
        return color.name();
    }

    public void setColor(Colors color) {
        this.color = color;
    }
}





